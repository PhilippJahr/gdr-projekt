//=================================================================
// TU Darmstadt, FB Informatik, FG SIM
// Robotik
//-----------------------------------------------------------------
// Student: <HIER BITTE IHREN NAMEN EINTRAGEN>
// Matrikelnummer: <UND HIER DIE MATRIKELNUMMER>
//=================================================================

//-----------------------------------------------------------------
//-- Die Funktion matNr gibt die angegebenen Matrikelnummer(n)
//   des/der abgebenden Studenten/Studentin auf die Konsole aus
//-----------------------------------------------------------------

#include <iostream>

using namespace std;

namespace turtlebot3
{
  static matNr(){    
    //-- Hier bitte als erstes Ihren vollständigen Namen und MatNr und danach die der anderen Gruppenmmitglieder eintragen!
    cout << "Name 1: " << "Philipp Jahr" << ", MatNr-1: " << 2727019 << endl;
    cout << "Name 2: " << "Julia Blome" << ", MatNr-2: " << 2539867 << endl;
  }
}
