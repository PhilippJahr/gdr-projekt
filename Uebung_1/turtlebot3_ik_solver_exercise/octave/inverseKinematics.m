%%-----------------------------------------------------------------
%%-- The method inverseKinematics computes the joint configurations
%%   suitable to reach the requested position and orientation in
%%   both elbow configurations.
%% 
%%   Input values are the endeffector position, the endeffector
%%   orientation with the yaw rotation about the z_0-axis and
%%   pitch angle between the z_0-axis and the endeffector direction
%%   described by x_n-axis.
%%-----------------------------------------------------------------
%%-- INPUT
%%   request_position       [3x1]  Requested endeffector position as [x, y, z]'
%%   py                     [2x1]  Requested pitch and yaw as [pitch, yaw]'
%%   l                      [Nx1]  Link lengths
%%
%%-- OUTPUT
%%   qs                     [2xN]  Resulting joint configurations or empty [] if
%%                                 no solution can be found
%%-----------------------------------------------------------------
function qs = inverseKinematics(request_position, py, l)
  % initialize return value
  qs = [];

  % | Implement your code here |
  % v                          v
  
  y = py(2);
  p = py(1);
  %R = [cos(-y), -sin(-y), 0;
  %     sin(-y), cos(-y), 0;
  %     0      , 0      , 1];
  %request_position = R*request_position;
  
  % q1
  qs(:,1) = [y; y];
  
  %q3
  c = [request_position(1)+cos(p-3*pi/2)*l(4); request_position(3)-l(1)-sin(p-3*pi/2)*l(4)];
  c2 = c(1)^2+c(2)^2;
  cosq3 = (c2-(l(3)^2)-(l(2)^2))/(2*l(3)*l(2));
  sinq3 = [+1;-1] * sqrt(1-cosq3^2);
  qs(:,3) = [atan2(sinq3(1), cosq3); atan2(sinq3(2), cosq3)];
  
  %q2
  alpha = asin(c(1)/sqrt(c2));
  beta = [atan2(l(3)*sinq3(1), l(2)+l(3)*sinq3(1));  atan2(l(3)*sinq3(2), l(2)+l(3)*sinq3(2))];
  qs(:,2) = -(pi/2-alpha-beta);
  
  %q4
  qs(:,4) = -p - pi/2 - qs(:,2) - qs(:,3);
  
  % ^                          ^
  % | -------- End ----------- |
end
