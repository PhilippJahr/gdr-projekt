%%=================================================================
%% TU Darmstadt, FB Informatik, FG SIM
%% Robotik
%%=================================================================

%%-----------------------------------------------------------------
%%-- Die Funktion matNr hat als Rueckgabewert die Matrikelnummer(n)
%%   des/der abgebenden Studenten/Studentin
%%-----------------------------------------------------------------


function m = matNr()
  %%-- Hier bitte als erstes die vollständigen Namen und Matrikelnummern aller Autoren eintragen!
  m = [{"Philipp Jahr",2727019};
       {"Julia Blome",2539867}];
end
